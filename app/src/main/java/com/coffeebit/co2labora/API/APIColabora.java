package com.coffeebit.co2labora.API;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.coffeebit.co2labora.VolleyCon;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nestor on 14/01/2016.
 */
public class APIColabora {

    private Context context;
    private String status = null;
    private String username;

    //private static final String BASE_URL = "http://192.168.5.196:8000/api/";
    private static final String BASE_URL = "http://coffeebit.us:3030";
    private SharedPreferences sp;

    public APIColabora(Context context){
        this.context = context;
        sp = context.getSharedPreferences("com.coffebit.co2labora", Context.MODE_PRIVATE);
    }

    public void logIn(String user, String pass, String identifier, final logInCallback callback){
        String url = BASE_URL + "/signIn";

        Map<String, String> map = new HashMap<>();
        map.put("email", user);
        map.put("password", pass);

        JSONObject jsonObject = new JSONObject(map);

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                        }catch(Exception ex){

                        }
                        callback.onSuccess(response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onFailure(error.networkResponse.statusCode);
                    }
                });
        //Add to singleton Volley queue
        VolleyCon.getInstance(this.context).addToRequestQueue(jsObjRequest);
    }

    public interface logInCallback{
        void onSuccess(JSONObject result);
        void onFailure(int statusCode);
    }

    public void register(String user, String pass, String email, String identifier, final registerCallback callback){
        String url = BASE_URL + "/signUp";

        Map<String, String> map = new HashMap<>();
        map.put("username", user);
        map.put("password", pass);
        map.put("email", email);
        if(identifier.isEmpty())
            map.put("userFb","0");
        else
            map.put("userFb","1");
        map.put("idFb",identifier);
        map.put("confirmate","1");

        JSONObject jsonObject = new JSONObject(map);


        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            callback.onSuccess(response);
                        }catch(Exception ex){

                        }
                        //callback.onSuccess(response);
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onFailure(error.networkResponse.statusCode);
                    }
                });
        //Add to singleton Volley queue
        VolleyCon.getInstance(this.context).addToRequestQueue(jsObjRequest);
    }

    public interface registerCallback{
        void onSuccess(JSONObject result) throws JSONException;
        void onFailure(int statusCode);
    }

    public void logOut(){
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.commit();
    }

}
