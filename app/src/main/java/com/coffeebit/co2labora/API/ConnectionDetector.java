package com.coffeebit.co2labora.API;

/**
 * Created by Nestor on 15/01/2016.
 */
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionDetector {

    private Context context;

    public ConnectionDetector(Context context){
        this.context = context;
    }

    public boolean isConnectedToInternet(){
        ConnectivityManager connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivity != null){
            NetworkInfo netInfo = connectivity.getActiveNetworkInfo();
            if((netInfo != null) && (netInfo.isConnected())){
                return true;
            }
        }
        return false;
    }
}
