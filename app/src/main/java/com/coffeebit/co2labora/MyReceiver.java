package com.coffeebit.co2labora;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Nestor on 14/01/2016.
 */
public class MyReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        String mensaje=intent.getExtras().getString("message");
        String title=intent.getExtras().getString("title");
        Intent service1 = new Intent(context, MyAlarmService.class);
        service1.putExtra("title",title);
        service1.putExtra("message",mensaje);
        context.startService(service1);
    }
}
