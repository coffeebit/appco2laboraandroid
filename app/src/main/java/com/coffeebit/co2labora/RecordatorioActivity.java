package com.coffeebit.co2labora;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class RecordatorioActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton back;
    private Button botonRecordatorio;
    private int contador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recordatorio);

        back=(ImageButton) findViewById(R.id.back_boton);
        back.setOnClickListener(this);

        botonRecordatorio=(Button) findViewById(R.id.boton_recordatorio);
        botonRecordatorio.setOnClickListener(this);

        contador++;
        //setRecordatorio();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.back_boton:
                finish();
                break;
            case R.id.boton_recordatorio:
                Intent intent = new Intent(RecordatorioActivity.this, EditActivity.class);
                startActivityForResult(intent, 1);
                break;
            default:
                break;

        }
    }

    public void setRecordatorio(String nombre, int hora, int minuto)
    {
        int hora12;
        String amPm;

        LinearLayout scroll = (LinearLayout)findViewById(R.id.main_menu_layout);
        View newcat = getLayoutInflater().inflate(R.layout.recordatorio, null);

        TextView nombreView=(TextView)newcat.findViewById(R.id.nombreRecordatorioView);
        nombreView.setText(nombre);

        if (hora==0) {
            hora12 = 12;
            amPm="am";
        }else if(hora>0 && hora<12){
            hora12 = hora;
            amPm="am";
        }else if (hora==12){
            hora12 = hora;
            amPm="pm";
        } else{
            hora12 = hora-12;
            amPm="pm";
        }

        TextView nombreTime=(TextView) newcat.findViewById(R.id.textTime);
        nombreTime.setText(String.valueOf(hora12+":"+String.valueOf(minuto)+" "+amPm));

        scroll.addView(newcat);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                int hora=data.getIntExtra("Hora",-1);
                int minutos=data.getIntExtra("Minutos",-1);
                String nombreGet=data.getStringExtra("Nombre");
                //Toast.makeText(getApplicationContext(),"Nombre: "+nombreGet,Toast.LENGTH_LONG).show();
                setRecordatorio(nombreGet,hora,minutos);
                Remind(hora,minutos,"Recordatorio",nombreGet);
            }
        }
    }

    public void Remind (int hora, int minuto, String title, String message)
    {
        contador ++;
        Intent alarmIntent = new Intent(getApplicationContext(), MyReceiver.class);
        alarmIntent.putExtra("message", message);
        alarmIntent.putExtra("title", title);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),contador,alarmIntent,0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        Calendar alarmCalendar=Calendar.getInstance();

        alarmCalendar.set(Calendar.HOUR_OF_DAY, hora);
        alarmCalendar.set(Calendar.MINUTE, minuto);
        alarmCalendar.set(Calendar.SECOND, 0);


        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, alarmCalendar.getTimeInMillis(), 24 * 60 * 60 * 1000, pendingIntent);  //set repeating every 24 hours


    }

}
