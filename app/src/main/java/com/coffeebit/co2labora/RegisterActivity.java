package com.coffeebit.co2labora;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.coffeebit.co2labora.API.APIColabora;
import com.coffeebit.co2labora.API.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private APIColabora apiColabora;
    private TextView nameText;
    private TextView emailText;
    private TextView passText;
    private TextView passText2;

    private Button botonRegistrarse;

    private ProgressDialog progressDialog;
    private SharedPreferences sp;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        context = getApplicationContext();

       // progressDialog=new ProgressDialog(this,R.style.AppThemeReloj);

        botonRegistrarse=(Button) findViewById(R.id.boton_registrarse);
        nameText = (TextView)findViewById(R.id.nombre);
        emailText = (TextView)findViewById(R.id.email);
        passText = (TextView)findViewById(R.id.passRegistro);
        passText2 = (TextView)findViewById(R.id.passConfirm);

        botonRegistrarse.setOnClickListener(this);


    }

    @Override
    protected void onStart() {
        super.onStart();
        sp = context.getSharedPreferences("com.coffebit.co2labora", Context.MODE_PRIVATE);
        apiColabora = new APIColabora(this);
    }

    public void registerUser(){

        ConnectionDetector cd = new ConnectionDetector(this);
        final String pass1=passText.getText().toString();
        final String pass2=passText2.getText().toString();
        final String name=nameText.getText().toString();
        final String emailTemp=emailText.getText().toString();
        if(name.equals("")||pass1.equals("") || pass2.equals("") || emailTemp.equals("")){
            Toast.makeText(this, "Ingrese la información requerida", Toast.LENGTH_SHORT).show();
        }else {
            if (pass1.equals(pass2)) {
           //     progressDialog.setProgressStyle(R.style.AppThemeReloj);
                if(cd.isConnectedToInternet()) {

                    progressDialog = ProgressDialog.show(RegisterActivity.this, "", "Iniciando sesión...", true);
                    //progressDialog.show(RegisterActivity.this, "", "Iniciando sesión...", true);

                    apiColabora.register(name, pass1, emailTemp, "", new APIColabora.registerCallback() {

                        @Override
                        public void onSuccess(JSONObject result) {
                            try {
                                JSONObject data = result.getJSONObject("data");

                                progressDialog.dismiss();

                                String resultado = data.getString("tag").toString();

                                if (resultado.equals("register")) {
                                    Toast.makeText(getApplicationContext(), "Registro exitoso!!", Toast.LENGTH_LONG).show();
                                    JSONArray arrayUser = data.getJSONArray("user");
                                    JSONObject user = arrayUser.getJSONObject(0);
                                    Toast.makeText(getApplicationContext(), user.getString("id").toString(), Toast.LENGTH_LONG).show();

                                    SharedPreferences.Editor e = sp.edit();
                                    e.putString("token", user.getString("id".toString()));
                                    e.putString("name", nameText.getText().toString());
                                    e.putString("email", emailText.getText().toString());
                                    e.putString("urlImagen", "");
                                    e.commit();
                                    Intent intent = new Intent(RegisterActivity.this, cuestionario1.class);
                                    startActivity(intent);
                                    finish();

                                } else {
                                    Toast.makeText(getApplicationContext(), "El usuario ya existe", Toast.LENGTH_LONG).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode) {
                            //Toast.makeText(getApplicationContext(), "Registrado", Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(), "Error al registrar", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }
                    });
                }else{
                    Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
                }
                //aqui
            } else {
                Toast.makeText(getApplicationContext(), "Las contraseñas no concuerdan", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.boton_registrarse:

                // LoginManager.getInstance().logOut();
                    registerUser();
                break;

            default:
                break;

        }
    }
}
