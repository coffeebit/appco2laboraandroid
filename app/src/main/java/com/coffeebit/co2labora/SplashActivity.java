package com.coffeebit.co2labora;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {
    Timer timer;
    private static final long DELAY_TIME = 1000;
    private static ImageView splash;
    private  SharedPreferences sp;
    private  String token;
    private  String cuestionario;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splash=(ImageView)findViewById(R.id.splash);

        context = getApplicationContext();
       // sp = getApplicationContext().getSharedPreferences("com.coffeebit.co2labora", Context.MODE_PRIVATE);





    }


    @Override
    protected void onStart() {
        super.onStart();
        sp = context.getSharedPreferences("com.coffebit.co2labora", Context.MODE_PRIVATE);

        token = sp.getString("token", null);
        cuestionario=sp.getString("cuestionario", null);


        TimerTask task = new TimerTask() {

            @Override
            public void run() {
                Intent i;
                if(token == null){
                    /*SharedPreferences.Editor e = sp.edit();
                    e.clear();
                    e.commit();*/
                    i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }else {
                    if(cuestionario==null) {
                        i = new Intent(SplashActivity.this, cuestionario1.class);
                        startActivity(i);
                    }else {
                        i = new Intent(SplashActivity.this, ResultActivity.class);
                        startActivity(i);
                    }
                    finish();
                }


            }
        };

        timer = new Timer();
        timer.schedule(task, DELAY_TIME);

    }

}
