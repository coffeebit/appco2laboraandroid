package com.coffeebit.co2labora;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.coffeebit.co2labora.API.APIColabora;
import com.coffeebit.co2labora.API.ConnectionDetector;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity implements OnClickListener {



    private EditText mPasswordView;
    private View mProgressView;
    private LoginButton loginButton;
    private TextView registerLabel;
    private CallbackManager callbackManager;
    private Button ourLoginButton;
    private Button loginFaceButton;

    private  String faceBookUser;
    private  String faceBookEmail;
    private  String faceBookURL;
    private  String faceBookId;

    private ProgressDialog progressDialog;

    private SharedPreferences sp;
    private static Intent i;
    private Context context;

    private APIColabora apiColabora;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_login);


        // Set up the login form.

        loginButton = (LoginButton) findViewById(R.id.login_button);
        ourLoginButton=(Button) findViewById(R.id.our_login_button);
        loginFaceButton=(Button) findViewById(R.id.loginFace);
        registerLabel=(TextView) findViewById(R.id.registerLabelClick);


        registerLabel.setOnClickListener(this);

        ourLoginButton.setOnClickListener(this);
        loginFaceButton.setOnClickListener(this);

        loginButton.setReadPermissions(Arrays.asList("public_profile, email"));

        callbackManager = CallbackManager.Factory.create();

        context = getApplicationContext();


        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                GraphRequest request=GraphRequest.newMeRequest(
                        loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject me, GraphResponse response) {
                                if (response.getError() != null) {
                                    // handle error
                                } else {
                                    faceBookEmail = me.optString("email");
                                    faceBookId = me.optString("id");
                                    faceBookUser=me.optString("name");
                                    faceBookURL="https://graph.facebook.com/" + faceBookId + "/picture?type=large";


                                    loginFaceBook();
                                    /*Intent intent = new Intent(LoginActivity.this, cuestionario1.class);
                                    startActivity(intent);


                                    Toast.makeText(LoginActivity.this, "Usuario: "+faceBookUser+" Email: "+faceBookEmail+ " URL: "+faceBookURL, Toast.LENGTH_LONG).show();*/
                                    //Toast.makeText(getApplicationContext(),"nombre: "+name+" email: "+email +" Object: "+me.toString(),Toast.LENGTH_LONG).show();
                                    // send email and id to your web server
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

    }


    @Override
    protected void onStart() {
        super.onStart();
        sp = context.getSharedPreferences("com.coffebit.co2labora", Context.MODE_PRIVATE);
        apiColabora = new APIColabora(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void loginForAPI(){
        ConnectionDetector cd = new ConnectionDetector(this);
        final TextView user = (TextView)findViewById(R.id.user);
        final TextView pass = (TextView)findViewById(R.id.password);
        if((user.getText().toString().equals(""))||(pass.getText().toString().equals(""))){
            Toast.makeText(this, "Ingrese la información requerida", Toast.LENGTH_SHORT).show();
        }else{
            if(cd.isConnectedToInternet()){

                progressDialog = ProgressDialog.show(LoginActivity.this, "", "", true);
                apiColabora.logIn(user.getText().toString(), pass.getText().toString(), "", new APIColabora.logInCallback() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        try {
                            JSONObject data = result.getJSONObject("data");

                            progressDialog.dismiss();
                           // Toast.makeText(getApplicationContext(),data.toString(),Toast.LENGTH_LONG).show();
                            boolean resultado = data.getBoolean("success");

                            if (resultado) {
                               // Toast.makeText(getApplicationContext(), "Sesion exitoso!!", Toast.LENGTH_LONG).show();
                                JSONArray arrayUser = data.getJSONArray("user");
                                JSONObject users = arrayUser.getJSONObject(0);

                                SharedPreferences.Editor e = sp.edit();
                                e.putString("token", users.getString("id".toString()));
                                e.putString("name", "");
                                e.putString("email", user.getText().toString());
                                e.putString("urlImagen", "");
                                e.putString("pass",pass.getText().toString());
                                e.commit();
                                Intent intent = new Intent(LoginActivity.this, cuestionario1.class);
                                startActivity(intent);
                                finish();

                            } else {
                                Toast.makeText(getApplicationContext(), "E-mail o contraseña incorrecta", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception ex) {
                            Toast.makeText(getApplicationContext(), "Intenta de nuevo", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    }
                    @Override
                    public void onFailure(int statusCode) {
                        Toast.makeText(getApplicationContext(), "Verifica tu usario y contraseña.", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });



            }else{
                Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public void loginFaceBook(){

        ConnectionDetector cd = new ConnectionDetector(this);
        final String pass1=faceBookEmail+".co2ffebit";

                //     progressDialog.setProgressStyle(R.style.AppThemeReloj);
                if(cd.isConnectedToInternet()) {

                    progressDialog = ProgressDialog.show(LoginActivity.this, "", "", true);
                    //progressDialog.show(RegisterActivity.this, "", "Iniciando sesión...", true);

                    apiColabora.register(faceBookUser, pass1, faceBookEmail, faceBookId, new APIColabora.registerCallback() {

                        @Override
                        public void onSuccess(JSONObject result) {
                            try {
                                JSONObject data = result.getJSONObject("data");

                                progressDialog.dismiss();

                                String resultado = data.getString("tag").toString();

                                if (resultado.equals("register")) {
                                   // Toast.makeText(getApplicationContext(), "Registro exitoso!!", Toast.LENGTH_LONG).show();
                                    JSONArray arrayUser = data.getJSONArray("user");
                                    JSONObject user = arrayUser.getJSONObject(0);
                                   // Toast.makeText(getApplicationContext(), user.getString("id").toString(), Toast.LENGTH_LONG).show();

                                    SharedPreferences.Editor e = sp.edit();
                                    e.putString("token", user.getString("id".toString()));
                                    e.putString("name", faceBookUser);
                                    e.putString("email", faceBookEmail);
                                    e.putString("urlImagen", faceBookURL);
                                    e.putString("pass",pass1);
                                    e.commit();


                                    Intent intent = new Intent(LoginActivity.this, cuestionario1.class);
                                    startActivity(intent);
                                    finish();

                                } else {
                                    //Toast.makeText(getApplicationContext(), "El usuario ya existe", Toast.LENGTH_LONG).show();


                                    apiColabora.logIn(faceBookEmail, pass1, faceBookId, new APIColabora.logInCallback() {
                                        @Override
                                        public void onSuccess(JSONObject result) {
                                            try {
                                                JSONObject data = result.getJSONObject("data");

                                                progressDialog.dismiss();
                                                // Toast.makeText(getApplicationContext(),data.toString(),Toast.LENGTH_LONG).show();
                                                boolean resultado = data.getBoolean("success");

                                                if (resultado) {
                                                    // Toast.makeText(getApplicationContext(), "Sesion exitoso!!", Toast.LENGTH_LONG).show();
                                                    JSONArray arrayUser = data.getJSONArray("user");
                                                    JSONObject users = arrayUser.getJSONObject(0);

                                                    SharedPreferences.Editor e = sp.edit();
                                                    e.putString("token", users.getString("id".toString()));
                                                    e.putString("name", faceBookUser);
                                                    e.putString("email", faceBookEmail);
                                                    e.putString("urlImagen", faceBookURL);
                                                    e.putString("pass", pass1);
                                                    e.commit();
                                                    Intent intent = new Intent(LoginActivity.this, cuestionario1.class);
                                                    startActivity(intent);
                                                    finish();

                                                } else {
                                                    Toast.makeText(getApplicationContext(), "error con facebook", Toast.LENGTH_LONG).show();
                                                }
                                            } catch (Exception ex) {
                                                Toast.makeText(getApplicationContext(), "Intenta de nuevo", Toast.LENGTH_SHORT).show();
                                                progressDialog.dismiss();
                                            }
                                        }

                                        @Override
                                        public void onFailure(int statusCode) {
                                            Toast.makeText(getApplicationContext(), "Verifica tu usario y contraseña.", Toast.LENGTH_SHORT).show();
                                            progressDialog.dismiss();
                                        }
                                    });
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode) {
                            //Toast.makeText(getApplicationContext(), "Registrado", Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(), "Error al registrar", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }
                    });
                }else{
                    Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_SHORT).show();
                }
                //aqui


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.our_login_button:
               // Toast.makeText(getApplicationContext(),sp.getString("token",null),Toast.LENGTH_LONG).show();
               // LoginManager.getInstance().logOut();
                loginForAPI();
               /* Intent intent = new Intent(this, cuestionario1.class);
                startActivity(intent);*/
                break;
            case R.id.registerLabelClick:
                Intent intentRegister = new Intent(this, RegisterActivity.class);
                startActivity(intentRegister);
                break;
            case R.id.loginFace:
               loginButton.performClick();
                break;
            default:
                break;

        }
    }
}

