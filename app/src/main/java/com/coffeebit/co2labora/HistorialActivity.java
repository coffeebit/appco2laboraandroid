package com.coffeebit.co2labora;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Nestor on 14/01/2016.
 */
public class HistorialActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);



        LinearLayout scroll = (LinearLayout)findViewById(R.id.main_menu_layout);

        back=(ImageButton) findViewById(R.id.back_boton);
        back.setOnClickListener(this);

        Button evaluar=(Button) findViewById(R.id.botonNuevaEvaluacion);
        evaluar.setOnClickListener(this);

        View historial1 = getLayoutInflater().inflate(R.layout.historial, null);
        TextView fecha1=(TextView) historial1.findViewById(R.id.fechaHistorial);
        TextView emicionesFecha1=(TextView) historial1.findViewById(R.id.emicionesDate);
        fecha1.setText("14 Enero de 2015");
        emicionesFecha1.setText("0.5 Ton CO2");


        View historial2 = getLayoutInflater().inflate(R.layout.historial, null);
        TextView fecha2=(TextView) historial2.findViewById(R.id.fechaHistorial);
        TextView emicionesFecha2=(TextView) historial2.findViewById(R.id.emicionesDate);
        fecha2.setText("14 Febrero de 2015");
        emicionesFecha2.setText("0.9 Ton CO2");

        scroll.addView(historial1);
        scroll.addView(historial2);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.back_boton:
                Intent home=new Intent(HistorialActivity.this,ResultActivity.class);
                startActivity(home);
                finish();
                break;
            case R.id.botonNuevaEvaluacion:
                Intent nueva=new Intent(HistorialActivity.this,cuestionario1.class);
                nueva.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(nueva);
                finish();
                break;
            default:
                break;

        }
    }

    @Override
    public void onBackPressed() {
        Intent home=new Intent(HistorialActivity.this,ResultActivity.class);
        startActivity(home);
        finish();
    }

}
