package com.coffeebit.co2labora;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Nestor on 14/01/2016.
 */
public class RankingActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        LinearLayout scroll = (LinearLayout)findViewById(R.id.main_menu_layout);

        back=(ImageButton) findViewById(R.id.back_boton);
        back.setOnClickListener(this);

        View ranking1 = getLayoutInflater().inflate(R.layout.ranking, null);
        TextView userPrimero=(TextView) ranking1.findViewById(R.id.userRanking);
        TextView emicionesPrimero=(TextView) ranking1.findViewById(R.id.emicionesUser);
        ImageView medallaPrimero=(ImageView) ranking1.findViewById(R.id.medalla);
        medallaPrimero.setImageDrawable(getResources().getDrawable(R.drawable.ranking_oro));
        userPrimero.setText("Nestor Cardenas");
        emicionesPrimero.setText("0.5 Ton CO2 por mes");

        View ranking2 = getLayoutInflater().inflate(R.layout.ranking, null);
        TextView userSegundo=(TextView) ranking2.findViewById(R.id.userRanking);
        TextView emicionesSegundo=(TextView) ranking2.findViewById(R.id.emicionesUser);
        ImageView medallaSegundo=(ImageView) ranking2.findViewById(R.id.medalla);
        medallaSegundo.setImageDrawable(getResources().getDrawable(R.drawable.ranking_plata));
        userSegundo.setText("Maturano melgoza");
        emicionesSegundo.setText("1.5 Ton CO2 por mes");


        View ranking3 = getLayoutInflater().inflate(R.layout.ranking, null);
        TextView userTercero=(TextView) ranking3.findViewById(R.id.userRanking);
        TextView emicionesTercero=(TextView) ranking3.findViewById(R.id.emicionesUser);
        ImageView medallaTercero=(ImageView) ranking3.findViewById(R.id.medalla);
        medallaTercero.setImageDrawable(getResources().getDrawable(R.drawable.ranking_bronce));
        userTercero.setText("Cesar Ivan Buenrostro");
        emicionesTercero.setText("2.5 Ton CO2 por mes");

        View ranking4 = getLayoutInflater().inflate(R.layout.ranking, null);
        TextView userCuarto=(TextView) ranking4.findViewById(R.id.userRanking);
        TextView emicionesCuarto=(TextView) ranking4.findViewById(R.id.emicionesUser);
        userCuarto.setText("Juan Pablo Cardenas");
        emicionesCuarto.setText("2.9 Ton CO2 por mes");

        scroll.addView(ranking1);
        scroll.addView(ranking2);
        scroll.addView(ranking3);
        scroll.addView(ranking4);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.back_boton:

                finish();
                break;

            default:
                break;

        }
    }
}
