package com.coffeebit.co2labora;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

public class cuestionario1 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private static ImageView imagenLuz;
    private static SeekBar seekBar1;
    private static TextView textViewKwh;
    private static int Kwh;
    private static double luzCO2;


    private static ImageView imagenAuto;
    private static SeekBar seekBar2;
    private static TextView textViewGasolina;
    private static int gasolina;
    private static double autoCO2;

    private static ImageView imagenGas;
    private static SeekBar seekBar3;
    private static TextView textViewGas;
    private static int gas;
    private static double gasCO2;


    private static ImageView imagenBasura;
    private static SeekBar seekBar4;
    private static TextView textViewPorcentaje;
    private static Button botonFinaizar;
    private static double basuraKilosCO2=30.381;
    private static double miBasuraCO2;

    private static double totalCO2;
    private static double hectarias;
    private static double porcentajeArboles;

    private static NavigationView navigationView;

    private static SharedPreferences sp;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuestionario1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        context = getApplicationContext();

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);



      /*  DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);*/


    }

    @Override
    protected void onStart() {
        super.onStart();
        sp = getSharedPreferences("com.coffebit.co2labora", Context.MODE_PRIVATE);

        //View header = LayoutInflater.from(this).inflate(R.layout.nav_header_cuestionario1, null);
        //navigationView.addHeaderView(header);
      /*  View header=navigationView.getHeaderView(0);

        TextView temp=(TextView) header.findViewById(R.id.userNameLabel);
        temp.setText(sp.getString("name",null));

        final ImageView userImage=(ImageView) header.findViewById(R.id.imageView);
        String lol = sp.getString("urlImagen", null);

        if (lol!=null) {
            ImageLoader imageLoader = VolleyCon.getInstance(this).getImageLoader();
            // Using normal ImageView
            imageLoader.get(lol, new ImageLoader.ImageListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //   Log.e(TAG, "Image Load Error: " + error.getMessage());
                   // userImage.setImageBitmap(ImageProfileHandler.getEllipseBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.profile), 1));
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    // TODO Auto-generated method stub
                    if (response.getBitmap() != null) {
                        Bitmap bm = ImageProfileHandler.getEllipseBitmap(response.getBitmap(),20);
                        userImage.setImageBitmap(bm);
                        //    userImage.setBackgroundResource(R.drawable.custom_user_image);
                    }
                }
            });
        }*/

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cuestionario1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_user) {
            Intent intent = new Intent(cuestionario1.this, ResultActivity.class);
            startActivity(intent);
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements View.OnClickListener {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {


            //View rootView = inflater.inflate(R.layout.swipe_layout, container, false);
           // Toast.makeText(getActivity(),String.valueOf(getArguments().getInt(ARG_SECTION_NUMBER)),Toast.LENGTH_SHORT).show();
           /* TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText("Que pedo "+String.valueOf(getArguments().getInt(ARG_SECTION_NUMBER)));*/

            View rootView;
            if (getArguments().getInt(ARG_SECTION_NUMBER)==1)
            {
                rootView = inflater.inflate(R.layout.swipe_layout, container, false);
                textViewKwh = (TextView) rootView.findViewById(R.id.textKwh);
                seekBar1=(SeekBar) rootView.findViewById(R.id.seekBar);
                imagenLuz=(ImageView) rootView.findViewById(R.id.imagen_luz);

                seekBar1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub
                    }
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        // TODO Auto-generated method stub

                         textViewKwh.setText(String.valueOf(progress));
                        Kwh=progress;

                        if (progress==0)
                        {
                            imagenLuz.setImageDrawable(getResources().getDrawable(R.drawable.luz_icon1));

                        }else if (progress>=1 && progress<200)
                        {
                            imagenLuz.setImageDrawable(getResources().getDrawable(R.drawable.luz_icon2));

                        }else if (progress>=200 && progress<400)
                        {
                            imagenLuz.setImageDrawable(getResources().getDrawable(R.drawable.luz_icon2));
                        }else if (progress>=400 && progress<600)
                        {
                            imagenLuz.setImageDrawable(getResources().getDrawable(R.drawable.luz_icon3));
                        }else if (progress>=600 && progress<800)
                        {
                            imagenLuz.setImageDrawable(getResources().getDrawable(R.drawable.luz_icon4));
                        }else if (progress>=800 && progress<1000)
                        {
                            imagenLuz.setImageDrawable(getResources().getDrawable(R.drawable.luz_icon5));
                        }else if (progress>=1000 && progress<1200)
                        {
                            imagenLuz.setImageDrawable(getResources().getDrawable(R.drawable.luz_icon6));
                        }else if (progress>=1200 && progress<1400)
                        {
                            imagenLuz.setImageDrawable(getResources().getDrawable(R.drawable.luz_icon7));
                        }else if (progress>=1400 && progress<1600)
                        {
                            imagenLuz.setImageDrawable(getResources().getDrawable(R.drawable.luz_icon8));
                        }else if (progress>=1600 && progress<1800)
                        {
                            imagenLuz.setImageDrawable(getResources().getDrawable(R.drawable.luz_icon9));
                        }else if (progress>=1800 && progress<=2000)
                        {
                            imagenLuz.setImageDrawable(getResources().getDrawable(R.drawable.luz_icon10));
                        }
                        //Toast.makeText(getApplicationContext(), String.valueOf(progress), Toast.LENGTH_LONG).show();

                    }
                });
                textViewKwh.setText("100");
                Kwh=100;
                imagenLuz.setImageDrawable(getResources().getDrawable(R.drawable.luz_icon2));
            }else if (getArguments().getInt(ARG_SECTION_NUMBER)==2){
                rootView = inflater.inflate(R.layout.swipe_layout2, container, false);

                textViewGasolina= (TextView) rootView.findViewById(R.id.textLitrosGas);
                seekBar2=(SeekBar) rootView.findViewById(R.id.seekBar2);
                imagenAuto=(ImageView) rootView.findViewById(R.id.imagen_auto);

                seekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub
                    }
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        // TODO Auto-generated method stub

                        textViewGasolina.setText(String.valueOf(progress));
                        gasolina=progress;

                        if (progress>=0 && progress<20)
                        {
                            imagenAuto.setImageDrawable(getResources().getDrawable(R.drawable.combustible_icon1));

                        }else if (progress>=20 && progress<40)
                        {
                            imagenAuto.setImageDrawable(getResources().getDrawable(R.drawable.combustible_icon2));
                        }else if (progress>=40 && progress<60)
                        {
                            imagenAuto.setImageDrawable(getResources().getDrawable(R.drawable.combustible_icon3));
                        }else if (progress>=60 && progress<80)
                        {
                            imagenAuto.setImageDrawable(getResources().getDrawable(R.drawable.combustible_icon4));
                        }else if (progress>=80 && progress<100)
                        {
                            imagenAuto.setImageDrawable(getResources().getDrawable(R.drawable.combustible_icon5));
                        }else if (progress>=100 && progress<120)
                        {
                            imagenAuto.setImageDrawable(getResources().getDrawable(R.drawable.combustible_icon6));
                        }else if (progress>=120 && progress<140)
                        {
                            imagenAuto.setImageDrawable(getResources().getDrawable(R.drawable.combustible_icon7));
                        }else if (progress>=140 && progress<160)
                        {
                            imagenAuto.setImageDrawable(getResources().getDrawable(R.drawable.combustible_icon8));
                        }else if (progress>=160 && progress<180)
                        {
                            imagenAuto.setImageDrawable(getResources().getDrawable(R.drawable.combustible_icon9));
                        }else if (progress>=180 && progress<=200)
                        {
                            imagenAuto.setImageDrawable(getResources().getDrawable(R.drawable.combustible_icon10));
                        }
                        //Toast.makeText(getApplicationContext(), String.valueOf(progress), Toast.LENGTH_LONG).show();

                    }
                });
                textViewGasolina.setText("40");
                imagenAuto.setImageDrawable(getResources().getDrawable(R.drawable.combustible_icon4));
                gasolina=40;
            }else if (getArguments().getInt(ARG_SECTION_NUMBER)==3){
                rootView = inflater.inflate(R.layout.swipe_layout3, container, false);

                textViewGas= (TextView) rootView.findViewById(R.id.textGas);
                seekBar3=(SeekBar) rootView.findViewById(R.id.seekBar3);
                imagenGas=(ImageView) rootView.findViewById(R.id.imagen_gas);

                seekBar3.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub
                    }
                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub
                    }
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        // TODO Auto-generated method stub

                        textViewGas.setText(String.valueOf(progress));
                        gas=progress;
                        //Toast.makeText(getApplicationContext(), String.valueOf(progress), Toast.LENGTH_LONG).show();
                        if (progress >= 0 && progress < 10) {
                            imagenGas.setImageDrawable(getResources().getDrawable(R.drawable.gas_icon1));
                        } else if (progress >= 10 && progress < 20) {
                            imagenGas.setImageDrawable(getResources().getDrawable(R.drawable.gas_icon2));
                        } else if (progress >= 20 && progress < 30) {
                            imagenGas.setImageDrawable(getResources().getDrawable(R.drawable.gas_icon3));
                        } else if (progress >= 30 && progress < 40) {
                            imagenGas.setImageDrawable(getResources().getDrawable(R.drawable.gas_icon4));
                        } else if (progress >= 40 && progress < 50) {
                            imagenGas.setImageDrawable(getResources().getDrawable(R.drawable.gas_icon5));
                        } else if (progress >= 50 && progress < 60) {
                            imagenGas.setImageDrawable(getResources().getDrawable(R.drawable.gas_icon6));
                        } else if (progress >= 60 && progress < 70) {
                            imagenGas.setImageDrawable(getResources().getDrawable(R.drawable.gas_icon7));
                        } else if (progress >= 70 && progress < 80) {
                            imagenGas.setImageDrawable(getResources().getDrawable(R.drawable.gas_icon8));
                        } else if (progress >= 80 && progress < 90) {
                            imagenGas.setImageDrawable(getResources().getDrawable(R.drawable.gas_icon9));
                        } else if (progress >= 90 && progress <= 100) {
                            imagenGas.setImageDrawable(getResources().getDrawable(R.drawable.gas_icon10));
                        }
                    }
                });
                textViewGas.setText("20");
                gas=20;
                imagenGas.setImageDrawable(getResources().getDrawable(R.drawable.gas_icon2));
            }else {
                rootView = inflater.inflate(R.layout.swipe_layout4, container, false);


                textViewPorcentaje= (TextView) rootView.findViewById(R.id.textPorciento);
                seekBar4=(SeekBar) rootView.findViewById(R.id.seekBar4);
                imagenBasura=(ImageView) rootView.findViewById(R.id.imagen_basura);
                botonFinaizar=(Button) rootView.findViewById(R.id.boton_finalizar);

                botonFinaizar.setOnClickListener(this);

                seekBar4.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        // TODO Auto-generated method stub

                        textViewPorcentaje.setText(String.valueOf(progress) + " %");

                        double tempProgress = progress;
                        tempProgress = (tempProgress - 100) * (-1);
                        miBasuraCO2 = basuraKilosCO2 * (tempProgress / 100);

                        if (progress >= 0 && progress < 10) {
                            imagenBasura.setImageDrawable(getResources().getDrawable(R.drawable.reciclar_icon1));

                        } else if (progress >= 10 && progress < 20) {
                            imagenBasura.setImageDrawable(getResources().getDrawable(R.drawable.reciclar_icon2));
                        } else if (progress >= 20 && progress < 30) {
                            imagenBasura.setImageDrawable(getResources().getDrawable(R.drawable.reciclar_icon3));
                        } else if (progress >= 30 && progress < 40) {
                            imagenBasura.setImageDrawable(getResources().getDrawable(R.drawable.reciclar_icon4));
                        } else if (progress >= 40 && progress < 50) {
                            imagenBasura.setImageDrawable(getResources().getDrawable(R.drawable.reciclar_icon5));
                        } else if (progress >= 50 && progress < 60) {
                            imagenBasura.setImageDrawable(getResources().getDrawable(R.drawable.reciclar_icon6));
                        } else if (progress >= 60 && progress < 70) {
                            imagenBasura.setImageDrawable(getResources().getDrawable(R.drawable.reciclar_icon7));
                        } else if (progress >= 70 && progress < 80) {
                            imagenBasura.setImageDrawable(getResources().getDrawable(R.drawable.reciclar_icon8));
                        } else if (progress >= 80 && progress < 90) {
                            imagenBasura.setImageDrawable(getResources().getDrawable(R.drawable.reciclar_icon9));
                        } else if (progress >= 90 && progress <= 100) {
                            imagenBasura.setImageDrawable(getResources().getDrawable(R.drawable.reciclar_icon10));
                        }
                        //Toast.makeText(getApplicationContext(), String.valueOf(progress), Toast.LENGTH_LONG).show();

                    }
                });
                textViewPorcentaje.setText("50" + " %");
                miBasuraCO2=basuraKilosCO2*(.5);
                imagenBasura.setImageDrawable(getResources().getDrawable(R.drawable.reciclar_icon5));
            }

          /*  */


            return rootView;
        }

        @Override
        public void onClick(View v) {
            //Toast.makeText(getActivity(),"Este es el mensaje",Toast.LENGTH_LONG).show();
            switch (v.getId())
            {
                case R.id.boton_finalizar:
                   // Toast.makeText(getActivity(),"Basura: " + String.valueOf(miBasuraCO2),Toast.LENGTH_LONG).show();

                    gasCO2=3*gas;
                    autoCO2=2.3*(gasolina*4);
                    luzCO2=.5827*(Kwh/2);

                    totalCO2=gasCO2+autoCO2+luzCO2+miBasuraCO2;// Emiciones totales al mes

                    hectarias=((118395054*(totalCO2*12))/1000)/6;

                    //Toast.makeText(getContext(),String.format("%.2f", hectarias),Toast.LENGTH_LONG).show();
                    porcentajeArboles=(hectarias)/1959247.98; //saber cuanto porcentaje cubren las hectarias necesarias en el territorio mexicano

                    float CO2Float= ((float) totalCO2);
                    float porcentaje= ((float) porcentajeArboles);
                    SharedPreferences.Editor e = sp.edit();
                    e.putString("cuestionario", "echo");
                    e.putFloat("CO2",CO2Float);
                    e.putFloat("porcentaje",porcentaje);
                    e.commit();

                    Intent intent = new Intent(getActivity(), ResultActivity.class);
                    //intent.putExtra("CO2", totalCO2);
                    //intent.putExtra("porcentaje", porcentajeArboles);

                    startActivity(intent);
                    getActivity().finish();
                    break;
                default:
                    break;

            }
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }
}


