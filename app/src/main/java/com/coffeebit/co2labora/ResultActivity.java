package com.coffeebit.co2labora;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.coffeebit.co2labora.API.APIColabora;
import com.facebook.login.LoginManager;

public class ResultActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private APIColabora apiColabora;
    private SharedPreferences sp;
    private static NavigationView navigationView;
    private static ImageView resultadoImagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
       // Toast.makeText(getApplicationContext()," Mexico: "+ String.format("%.0f", totalEmiciones),Toast.LENGTH_LONG).show();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }



    @Override
    protected void onStart() {
        super.onStart();
        sp = getSharedPreferences("com.coffebit.co2labora", Context.MODE_PRIVATE);

        float CO2= sp.getFloat("CO2",0);
        float porcentaje= sp.getFloat("porcentaje", 0);

        float CO2Tonelada=CO2 / 1000;

        apiColabora = new APIColabora(this);
        TextView resul=(TextView) findViewById(R.id.labelResult);
        resul.setText(String.format("%.1f", CO2Tonelada) + " " + getResources().getString(R.string.string_emiciones));
        TextView resul2=(TextView) findViewById(R.id.labelResultArbol);
        resul2.setText(getResources().getString(R.string.string_porcentaje1)+" "+String.format("%.0f", porcentaje)+ getResources().getString(R.string.string_porcentaje2));

        resultadoImagen=(ImageView) findViewById(R.id.imagenResultado);

        if (porcentaje>=100)
        {
            resultadoImagen.setImageDrawable(getResources().getDrawable(R.drawable.resultado_contaminado));
        }else if(porcentaje>=50 && porcentaje<100)
        {
            resultadoImagen.setImageDrawable(getResources().getDrawable(R.drawable.resultado_mediocontaminado));
        }else if(porcentaje<50)
        {
            resultadoImagen.setImageDrawable(getResources().getDrawable(R.drawable.resultado_imagen));
        }
        //View header = LayoutInflater.from(this).inflate(R.layout.nav_header_cuestionario1, null);
        //navigationView.addHeaderView(header);
        final View header=navigationView.getHeaderView(0);

        TextView temp=(TextView) header.findViewById(R.id.userNameLabel);
        temp.setText(sp.getString("name",null));

        final ImageView userImage=(ImageView) header.findViewById(R.id.imageView);
        String lol = sp.getString("urlImagen", null);

        final ImageView headerImage=(ImageView) header.findViewById(R.id.headerImagen);


        if (lol!=null) {
            ImageLoader imageLoader = VolleyCon.getInstance(this).getImageLoader();
            // Using normal ImageView
            imageLoader.get(lol, new ImageLoader.ImageListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //   Log.e(TAG, "Image Load Error: " + error.getMessage());
                   // userImage.setImageBitmap(ImageProfileHandler.getEllipseBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.profile), 1));
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    // TODO Auto-generated method stub
                    if (response.getBitmap() != null) {
                        Bitmap bm = ImageProfileHandler.getEllipseBitmap(response.getBitmap(),20);
                        headerImage.setImageBitmap(response.getBitmap());
                        userImage.setImageBitmap(bm);
                        //    userImage.setBackgroundResource(R.drawable.custom_user_image);
                    }
                }
            });
        }

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.result, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_perfil) {
            // Handle the camera action
            Intent perfil=new Intent(ResultActivity.this,PerfilActivity.class);
            startActivity(perfil);
        } else if (id == R.id.nav_historial) {
            Intent historial=new Intent(ResultActivity.this,HistorialActivity.class);
            startActivity(historial);
            finish();
        } else if (id == R.id.nav_ranking) {
            Intent ranking=new Intent(ResultActivity.this,RankingActivity.class);
            startActivity(ranking);
        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        } else if (id == R.id.nav_recordatorios)
        {
            Intent recordatorio=new Intent(ResultActivity.this,RecordatorioActivity.class);
            startActivity(recordatorio);
        }else if (id== R.id.nav_configuraciones)
        {
            Intent configuraciones=new Intent(ResultActivity.this,ConfiguracionActivity.class);
            startActivity(configuraciones);
        }else if (id==R.id.nav_logout)
        {
            //Toast.makeText(getApplicationContext(),"vergas",Toast.LENGTH_SHORT).show();
            apiColabora.logOut();
            LoginManager.getInstance().logOut();
            Intent regresar=new Intent(ResultActivity.this,LoginActivity.class);
            startActivity(regresar);
            finish();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
