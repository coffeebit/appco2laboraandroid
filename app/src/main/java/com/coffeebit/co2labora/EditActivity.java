package com.coffeebit.co2labora;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;
import android.widget.Toast;

public class EditActivity extends AppCompatActivity implements View.OnClickListener{

    private TimePicker selector;
    private Button botonGuardar;
    private EditText nombre;
    private ImageButton back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        selector=(TimePicker) findViewById(R.id.timePicker);

        back=(ImageButton) findViewById(R.id.back_boton);
        back.setOnClickListener(this);


        botonGuardar=(Button) findViewById(R.id.boton_guardar);
        botonGuardar.setOnClickListener(this);

        nombre=(EditText) findViewById(R.id.nombreRecordatorio);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cuestionario1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_user) {

            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.boton_guardar:
               // Toast.makeText(getApplicationContext(),"Hora: "+String.valueOf(selector.getCurrentHour())+" Minutos: "+nombre.getText(),Toast.LENGTH_LONG).show();
                Intent intent = new Intent();
                intent.putExtra("Hora",selector.getCurrentHour());
                intent.putExtra("Minutos",selector.getCurrentMinute());
                intent.putExtra("Nombre",nombre.getText().toString());
                setResult(RESULT_OK, intent);
                finish();
                break;
            case R.id.boton_recordatorio:

                break;
            case R.id.back_boton:
                finish();
                break;
            default:
                break;

        }
    }
}
