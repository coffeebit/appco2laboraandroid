package com.coffeebit.co2labora;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Nestor on 14/01/2016.
 */
public class ConfiguracionActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageButton back;
    private Button botonRecordatorio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);

       back=(ImageButton) findViewById(R.id.back_boton);
        back.setOnClickListener(this);


        LinearLayout scroll = (LinearLayout)findViewById(R.id.main_menu_layout);
        View configuracionesObject = getLayoutInflater().inflate(R.layout.configuracion, null);
        TextView recordatorios=(TextView) configuracionesObject.findViewById(R.id.nombreConfiguracionView);
        recordatorios.setText("Recordatorios");

        View notificacionesObject = getLayoutInflater().inflate(R.layout.configuracion, null);
        TextView notificaciones=(TextView) notificacionesObject.findViewById(R.id.nombreConfiguracionView);
        notificaciones.setText("Notificaciones inteligentes");

        View tipsObject = getLayoutInflater().inflate(R.layout.configuracion, null);
        TextView tips=(TextView) tipsObject.findViewById(R.id.nombreConfiguracionView);
        tips.setText("Tips");

        View evaluarObject = getLayoutInflater().inflate(R.layout.configuracion, null);
        TextView evaluar=(TextView) evaluarObject.findViewById(R.id.nombreConfiguracionView);
        evaluar.setText("Evaluar cada mes");

        scroll.addView(configuracionesObject);
        scroll.addView(notificacionesObject);
        scroll.addView(tipsObject);
        scroll.addView(evaluarObject);






    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.back_boton:

                finish();
                break;

            default:
                break;

        }
    }



}
