package com.coffeebit.co2labora;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

public class PerfilActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView back;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        back=(ImageView) findViewById(R.id.backImagen);
        back.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        sp = getSharedPreferences("com.coffebit.co2labora", Context.MODE_PRIVATE);


        TextView tempNombre=(TextView) findViewById(R.id.textNombre);
        tempNombre.setText(sp.getString("name",null));

        TextView tempEmail=(TextView) findViewById(R.id.nombreEmail);
        tempEmail.setText(sp.getString("email",null));

        TextView tempPass=(TextView) findViewById(R.id.contrasena);
        tempPass.setText(sp.getString("pass",null));

        TextView tempDireccion=(TextView) findViewById(R.id.direccionText);
        tempDireccion.setText(sp.getString("direccion",null));

        TextView nombreUsuario=(TextView) findViewById(R.id.nombreUsuario);
        nombreUsuario.setText(sp.getString("name",null));



        final ImageView userImage=(ImageView) findViewById(R.id.perfilImagen);
        final ImageView headerImage=(ImageView) findViewById(R.id.headerImagen);

        String lol = sp.getString("urlImagen", null);

        if (lol!=null) {
            ImageLoader imageLoader = VolleyCon.getInstance(this).getImageLoader();
            // Using normal ImageView
            imageLoader.get(lol, new ImageLoader.ImageListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //   Log.e(TAG, "Image Load Error: " + error.getMessage());
                    // userImage.setImageBitmap(ImageProfileHandler.getEllipseBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.profile), 1));
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    // TODO Auto-generated method stub
                    if (response.getBitmap() != null) {
                        Bitmap bm = ImageProfileHandler.getEllipseBitmap(response.getBitmap(),20);
                        headerImage.setImageBitmap(response.getBitmap());
                        userImage.setImageBitmap(bm);
                        //    userImage.setBackgroundResource(R.drawable.custom_user_image);
                    }
                }
            });
        }

    }

    @Override
    public void onClick(View v) {
            switch (v.getId())
            {
                case R.id.backImagen:
                    finish();
                    break;

                default:
                    break;

            }

    }
}
