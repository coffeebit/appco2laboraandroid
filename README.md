# README #

### ¿Qué es Colabora? ###

* La aplicación en android apoya el concepto verde y el cuidado de nuestro planeta. Su principal función es informar a las personas acerca de cómo influye la presencia del Dióxido de Carbono en nuestra atmósfera, así como informar las medidas necesarias para ayudar a nuestro planeta y al mismo tiempo beneficiar a nuestra economía. Con la ayuda de un asistente personal dentro de la aplicación, recordatorios y tips seguramente podremos crear una relación Ganar - Ganar, ayudamos a nuestro planeta y ahorramos dinero.
* Prototipo V1.0
* http://co2labora.coffeebit.us/

### ¿Cómo lo pruebo? ###

En la raíz del repositorio  se encuentra el archivo .apk para ser probado (Funciones limitadas por ser prototipo)
